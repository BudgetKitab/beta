import el from './../js/elements.js';

async function fetchLangJson() {
  return fetch('lang/lang.json')
    .then(response => response.json());
}

function setupLangChoiceListeners(langJson) {
  const langMenu = document.querySelectorAll('#lang-choice md-menu-item');
  const langList = document.querySelectorAll('#lang-content md-list-item');

  const langSelectElements = [...langMenu, ...langList];

  langSelectElements.forEach(item => {
    item.addEventListener('click', () => {
      const selectedLang = item.getAttribute('value');
      localStorage.setItem('chosenLanguage', JSON.stringify(selectedLang));
      setLangTexts(langJson, selectedLang);
      el.langDialog.close();
    });
  });
}

function setLangTexts(langJson, selectedLang) {
  Object.entries(langJson[selectedLang]).forEach(([key, { text, target }]) => {
    document.getElementById(key)[target] = text;
  });
}

function initLang(langJson) {
  const storedLang = localStorage.getItem('chosenLanguage');
  if (storedLang) {
    setLangTexts(langJson, JSON.parse(storedLang));
  } else {
    el.langDialog.show();
    console.log("No stored language found. Using default language.");
  }
}

fetchLangJson().then(langJson => {
  initLang(langJson);
  setupLangChoiceListeners(langJson);
});