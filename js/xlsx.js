import el from './elements.js';

el.downloadData.addEventListener('click', () => {
  const yearlyEntries = JSON.parse(localStorage.getItem('yearlyEntries'));

  const consolidatedData = Object.values(yearlyEntries).flat().map((entry) => ({
    Year: Object.keys(yearlyEntries).find((year) => yearlyEntries[year].includes(entry)),
    ...entry
  }));

  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, XLSX.utils.json_to_sheet(consolidatedData), 'Data');

  const fileName = `BudgetKitab-${new Date().toISOString().slice(0, 10)}.xlsx`;
  XLSX.writeFile(workbook, fileName, { compression: true });
});