// state.js
const original = localStorage.setItem;

localStorage.setItem = function (key, value) {
    const event = new Event('localStorageUpdated');

    event.key = key;
    event.value = value;

    document.dispatchEvent(event);

    original.apply(this, arguments);
};

const yearSelect = document.getElementById('year-select');

yearSelect.addEventListener('change', (e) => {
  const event = new CustomEvent('yearSelectChanged', {
    detail: {
      newValue: e.target.value
    }
  });
  document.dispatchEvent(event);
});

