const el = {
    loading: document.getElementById('loading-overlay'),
    fab: document.getElementById('fab'),
    success: document.getElementById('success'),
    
    header: document.getElementById('header'),
    mainIcon: document.getElementById('main-icon'),

    installTopButton: document.getElementById('install-top-button'),
    menuButton: document.body.querySelector('#menu-button'),
    menuList: document.body.querySelector('#menu-list'),

    addBudget: document.getElementById('add-budget'),
    downloadData: document.getElementById('download-data'),

    budgetNumber: document.getElementById('budget-number'),
    additionalBudgetEl: document.getElementById('additional-budget-number'),
    budgetBar: document.getElementById('budget-bar'),
    yearSelect: document.getElementById('year-select'),
    
    bookList: document.getElementById('book-list'),
    detailIcon: document.getElementById('detail-icon'),
    
    addBookDialog: document.getElementById('add-book-dialog'),
    addBookForm: document.getElementById('add-book-form'),
    saveButton: document.getElementById('save-form'),
    deleteButton: document.getElementById('delete-book'),
    editButton: document.getElementById('edit-book'),

    bookDetailDialog: document.getElementById('book-detail-dialog'),
    bookDetailForm: document.getElementById('book-detail-form'),
    dateButton: document.getElementById('date-button'),
    dateInput: document.getElementById('input-date'),
    inputProxyImage: document.getElementById('proxy-image'),
    imageButton: document.getElementById('image-button'),
    bookDetailImage: document.getElementById('book-detail-image'),
    imageIcon: document.querySelector('#image-button md-icon'),
    imageInput: document.getElementById('input-image'),
    inputWishlist: document.getElementById('input-wishlist'),
    chipWishlist: document.getElementById('chip-wishlist'),
    inputCopy: document.getElementById('input-copy'),
    chipCopy: document.getElementById('chip-copy'),
    
    about: document.getElementById('about'), 

    installDialog: document.getElementById('install-dialog'),
    installButton: document.getElementById('install-button'),
    installAction: document.getElementById('install-action'),
    installInstruction: document.getElementById('instruction'),

    langDialog: document.getElementById('language-dialog'),
};

export default el;