import el from './elements.js'

const appIcon = el.installDialog.querySelector('img');

let deferredPrompt = null;

if ('onbeforeinstallprompt' in window) {
  console.log('Browser supports beforeinstallprompt event');
  window.addEventListener('beforeinstallprompt', (e) => {
    el.installTopButton.style.display = 'flex';
    appIcon.src = 'images/icons/play.svg'
    e.preventDefault();
    deferredPrompt = e;
    showInstallDialog();
  });
} else {
  console.log('Browser does not support beforeinstallprompt event');
  appIcon.src = 'images/icons/appstore.svg';
  el.installInstruction.style.display = 'flex';
  el.installTopButton.style.display = 'flex';
  el.installAction.style.display = 'none';
  
  showInstallDialog()
}

function showInstallDialog() {
  if (el.langDialog.hasAttribute('open')) {
    el.langDialog.addEventListener('closed', function() {
      el.installDialog.show();
    });
  } else {
    el.installDialog.show();
  }
}

el.installTopButton.addEventListener('click', showInstallDialog);

el.installButton.addEventListener('click', async () => {
  deferredPrompt.prompt();
  const { outcome } = await deferredPrompt.userChoice;
  if (outcome === 'accepted') {
    console.log('User accepted the install prompt.');
    el.installDialog.close();
  } else if (outcome === 'dismissed') {
    console.log('User dismissed the install prompt');
  }
  deferredPrompt = null;
});

window.addEventListener('appinstalled', () => {
  el.installTopButton.style.display = 'none';
});

function getPWADisplayMode() {
  if (document.referrer.startsWith('android-app://'))
    return 'twa';
  if (window.matchMedia('(display-mode: browser)').matches)
    return 'browser';
  if (window.matchMedia('(display-mode: standalone)').matches)
    return 'standalone';
  if (window.matchMedia('(display-mode: minimal-ui)').matches)
    return 'minimal-ui';
  if (window.matchMedia('(display-mode: fullscreen)').matches)
    return 'fullscreen';
  if (window.matchMedia('(display-mode: window-controls-overlay)').matches)
    return 'window-controls-overlay';

  return 'unknown';
}

window.addEventListener('load', () => {
  const displayMode = getPWADisplayMode();
  console.log('Launched on:', displayMode);

  if (displayMode === 'standalone') {
    el.installTopButton.style.display = 'none';
  }
});