module.exports = {
	globDirectory: './',
	globPatterns: [
		'**/*.{webmanifest,css,ico,svg,png,webp,html,js,json}'
	],
	globIgnores: ['**/images/screenshots/**/*'],
	clientsClaim: true,
	skipWaiting: true,
	swDest: 'sw.js',
	runtimeCaching: [
		{
			urlPattern: /^https:\/\/fonts\.googleapis\.com\/css2\?.*/,
			handler: 'StaleWhileRevalidate',
			options: {
				cacheName: 'google-fonts-stylesheets',
				expiration: {
					maxAgeSeconds: 31536000
				},
				cacheableResponse: {
					statuses: [0, 200]
				}
			}
		},
		{
			urlPattern: /^https:\/\/fonts\.googleapis\.com\/icon\?.*/,
			handler: 'StaleWhileRevalidate',
			options: {
				cacheName: 'google-fonts-icons',
				expiration: {
					maxAgeSeconds: 31536000
				},
				cacheableResponse: {
					statuses: [0, 200]
				}
			}
		},
		{
			urlPattern: /^https:\/\/cdn.sheetjs.com\/xlsx-0.20.3\/package\/dist\/xlsx.full.min.js$/,
			handler: 'CacheFirst',
			options: {
				cacheName: 'xlsx-library',
				expiration: {
					maxAgeSeconds: 31536000
				},
				cacheableResponse: {
					statuses: [0, 200]
				}
			}
		},
		{
			urlPattern: /^https:\/\/unpkg.com\/dexie\/dist\/modern\/dexie.min.mjs$/,
			handler: 'CacheFirst',
			options: {
				cacheName: 'dexie-library',
				expiration: {
					maxAgeSeconds: 31536000
				},
				cacheableResponse: {
					statuses: [0, 200]
				}
			}
		},
	]
};